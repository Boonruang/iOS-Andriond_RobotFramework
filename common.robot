*** Settings ***
Library    AppiumLibrary
Resource    Resource/iOS/variables.robot
Resource    Resource/iOS/keyword_login.robot
Resource    Resource/iOS/keyword_setup.robot
Resource    Resource/iOS/keyword_checklist.robot
Resource    Resource/iOS/keyword_product.robot
Resource    Resource/iOS/keyword_mydata.robot
Resource    Resource/iOS/keyword_profile.robot
Resource    Resource/iOS/keyword_register.robot

Resource    Resource/Android/variables.robot
Resource    Resource/Android/keyword_login_android.robot
Resource    Resource/Android/keyword_setup_android.robot
Resource    Resource/Android/keyword_register_android.robot
Resource    Resource/Android/keyword_checklist_android.robot
Resource    Resource/Android/keyword_product_android.robot
Resource    Resource/Android/keyword_mydata_android.robot
Resource    Resource/Android/keyword_profile_android.robot