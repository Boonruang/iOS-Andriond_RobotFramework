*** Settings ***
Resource    ../../common.robot
Test Setup  Open Application    ${localhost}
...  platformName=${platformName}
...  platformVersion=${platformVersion}
...  deviceName=${deviceName}
...  appPackage=${appPackage=com}
...  app=${appLocation}
...  unicodeKeyboard=true
...  appWaitPackage=${appPackage=com}
...  fullReset=true
Test Teardown  Close Application

*** Test Cases ***

Login : Login with valid email&password should success
     Login Android   ${antxt_email}    ${antxt_password}     ${anmsg_success}
Logout : Member logout should success
    Logout Android  ${antxt_email}    ${antxt_password}     ${anmsg_success}
Login : Login with invalid email should fail
    Login Android   xxxx@test.com    ${antxt_password}     ${anmsg_fail}
Login : Login with invalid password should fail
    Login Android   ${antxt_email}    xxxxx     ${anmsg_fail}
Login : Login null email should fail
    Login Android   ${empty}    ${antxt_password}     ${anmsg_fail}
Login : Login null password should fail
    Login Android   ${antxt_email}    ${empty}     ${anmsg_fail}
Login : Login null email&password should fail
    Login Android   ${empty}    ${empty}     ${anmsg_fail}
