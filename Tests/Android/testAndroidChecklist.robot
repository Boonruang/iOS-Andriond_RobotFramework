*** Settings ***
Resource    ../../common.robot
Test Setup  Open Application    ${localhost}
...  platformName=${platformName}
...  platformVersion=${platformVersion}
...  deviceName=${deviceName}
...  appPackage=${appPackage=com}
...  app=${appLocation}
...  unicodeKeyboard=true
...  appWaitPackage=${appPackage=com}
...  fullReset=true
Test Teardown  Close Application

*** Test Cases ***

Checklist: buy checklist without login
    buy checklist without login
Checklist: buy checklist and Does checklist
  buy checklist android
Checklist : Do checklist but not complete
   buy checklist not complete android
Checklist : When comeback to app should show checklist
   comback does checklist not complete android
