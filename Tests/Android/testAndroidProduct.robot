*** Settings ***
Resource    ../../common.robot
Test Setup  Open Application    ${localhost}
...  platformName=${platformName}
...  platformVersion=${platformVersion}
...  deviceName=${deviceName}
...  appPackage=${appPackage=com}
...  app=${appLocation}
...  unicodeKeyboard=true
...  appWaitPackage=${appPackage=com}
...  fullReset=true
Test Teardown  Close Application

*** Test Cases ***

Product : Buy package
    buy product success
Product : Add product to cart
    add product to cart android
Product : Remove product from chart
    remove product from cart android
Product : Checkout product without address
    checkout with no address android
