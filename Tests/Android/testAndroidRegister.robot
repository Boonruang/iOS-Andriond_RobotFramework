*** Settings ***
Resource    ../../common.robot
Test Setup  Open Application    ${localhost}
...  platformName=${platformName}
...  platformVersion=${platformVersion}
...  deviceName=${deviceName}
...  appPackage=${appPackage=com}
...  app=${appLocation}
...  unicodeKeyboard=true
...  appWaitPackage=${appPackage=com}
...  fullReset=true
Test Teardown  Close Application

*** Test Cases ***

Register : Guest register should sucess
    Register Android Complete
Register : Gust register does't fill field should fail
    Register Android does't fill field
Register : Guest register dobplicate email should fail
    Register Android duplicate email
Register : Guest register with password less than 6 digit should fail
    Register Android with password less than 6 digit
Register : Guest register with password more than 12 digit should fail
    Register Android with password more than 12 digit
Register : Guest register with invalid type email should fail
    Register Android with invalid type email
Register : Guset register with doesn't fill email should fail
    Register Android with doesn't fill email
Register : Guset register with doesn't fill password should fail
    Register Android with doesn't fill password
Register : Guset register with doesn't fill email&password should fail
    Register Android with doesn't fill email&password
