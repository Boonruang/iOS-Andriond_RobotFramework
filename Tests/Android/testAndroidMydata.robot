*** Settings ***
Resource    ../../common.robot
Test Setup  Open Application    ${localhost}
...  platformName=${platformName}
...  platformVersion=${platformVersion}
...  deviceName=${deviceName}
...  appPackage=${appPackage=com}
...  app=${appLocation}
...  unicodeKeyboard=true
...  appWaitPackage=${appPackage=com}
...  fullReset=true
Test Teardown  Close Application

*** Test Cases ***

Mydata : Menu mydata without login
    menu mydata without login android
Mydata : Menu mydata with login but haven't data
    check mydata with login have't data android
Mydata : Menu mydata with first checklist finish
    first enter mydata menu android
Mydata : Test search and cheange category in Mydata
    search state mydata menu android

Profile : Member Update profile
    update profile
#Profile : Member add adress
#    add address android
Profile : Member check order
    check order in profile menu
