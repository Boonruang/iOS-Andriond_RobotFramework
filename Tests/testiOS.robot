*** Settings ***
Resource    ../common.robot

*** Test Case ***
#Login iOS: Login with invalid email&password should success
#    Login   ${txt_email}    ${txt_password}     ${msg_success}
#Login iOS: Login with invalid email should fail
#    Login   testeee@test.com    ${txt_password}     ${msg_fail}
#Login iOS: Login with invalid password should fail
#    Login   ${txt_email}    xxxxx     ${msg_fail}
#Login iOS: Login null email should fail
#    Login   ${empty}    ${txt_password}     ${msg_fail}
#Login iOS: Login null password should fail
#    Login   ${txt_email}    ${empty}     ${msg_fail}
#Login iOS: Login null email&password should fail
#    Login   ${empty}    ${empty}     ${msg_fail}
#Logout iOS: Logout should success
#    Logout

#Checklist : Show detail checklist should success
#    show detail checklist
#Checklist : Buy checklist with cheked Term&condition should success
#    checked term&condition
#Checklist : Buy checklist without check Term&condition should success
#    without check term&condition
Checklist : Buy checklist should success
    buy checklist
#Checklist : Buy checklist but not complete should success
#    buy checklist not complete
#Checklist : When comeback to app with not complte checklist should success
#    buy checklist but doesn't checklist
#Checklist : Buy first checklist should be show page continue in mydata menu
#    buy first checklist
#
#Product : show detail product&package should success
#    show detail product&package
#Product : add product to cart should success
#    add product to cart
#Product : remove product from cart should success
#    remove product to cart
#Product : checkout product with no address should success
#    checkout with no address
#Product : checkout product with address should success
#    checkout with address
#
#Mydata : check menu mydata without login
#    menu mydata without login
#Mydata : check menu mydata with login have't data
#    check mydata with login have't data
#Mydata : show first detail mydata should success
#    first enter mydata menu
#Mydata : show seconde detail mydata should success
#    seconde enter mydata menu
#Mydata : search state menu mydata should success
#    search state mydata menu
#Mydata : change category in mydata menu should success
#    change category in mydata menu
#Mydata : check button hide in mydata
#    button hide in mydata
##
#Profile : Update Information
#    update information user
#Profile : Add address
#    add address in profile
#
#Register : Guest register should sucess
#    Register Complete
#Register : Guest register without sex should fail
#    Register without sex
#Register : Guest register not fill field should fail
#    Register not fill field
#Register : Guest register duplicate email
#    Reigster duplicate email