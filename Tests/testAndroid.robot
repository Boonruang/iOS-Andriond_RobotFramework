*** Settings ***
Resource    ../common.robot
Test Setup  Open Application    ${localhost}
...  platformName=${platformName}
...  platformVersion=${platformVersion}
...  deviceName=${deviceName}
...  appPackage=${appPackage=com}
...  app=${appLocation}
...  unicodeKeyboard=true
...  appWaitPackage=${appPackage=com}
...  appWaitActivity=com.twentyscoops.arbotena.ui.splashscreen.SplashScreenActivity
...  appWaitActivity=com.twentyscoops.arbotena.ui.main.MainActivity

Test Teardown  Close Application

*** Test Cases ***

Checklist: buy checklist without login
    buy checklist without login
Checklist: buy checklist and Does checklist
  buy checklist android
Checklist : Do checklist but not complete
   buy checklist not complete android
Checklist : When comeback to app should show checklist
   comback does checklist not complete android

Product : Buy package
    buy product success
Product : Add product to cart
    add product to cart android
Product : Remove product from chart
    remove product from cart android
Product : Checkout product without address
    checkout with no address android

Mydata : Menu mydata without login
    menu mydata without login android
Mydata : Menu mydata with login but haven't data
    check mydata with login have't data android
Mydata : Menu mydata with first checklist finish
    first enter mydata menu android
Mydata : Test search and cheange category in Mydata
    search state mydata menu android

Profile : Member Update profile
    update profile
#Profile : Member add adress
#    add address android
Profile : Member check order
    check order in profile menu


Login : Login with valid email&password should success
     Login Android   ${antxt_email}    ${antxt_password}     ${anmsg_success}
Logout : Member logout should success
    Logout Android  ${antxt_email}    ${antxt_password}     ${anmsg_success}
Login : Login with invalid email should fail
    Login Android   xxxx@test.com    ${antxt_password}     ${anmsg_fail}
Login : Login with invalid password should fail
    Login Android   ${antxt_email}    xxxxx     ${anmsg_fail}
Login : Login null email should fail
    Login Android   ${empty}    ${antxt_password}     ${anmsg_fail}
Login : Login null password should fail
    Login Android   ${antxt_email}    ${empty}     ${anmsg_fail}
Login : Login null email&password should fail
    Login Android   ${empty}    ${empty}     ${anmsg_fail}

Register : Guest register should sucess
    Register Android Complete
Register : Gust register does't fill field should fail
    Register Android does't fill field
Register : Guest register dobplicate email should fail
    Register Android duplicate email
Register : Guest register with password less than 6 digit should fail
    Register Android with password less than 6 digit
Register : Guest register with password more than 12 digit should fail
    Register Android with password more than 12 digit
Register : Guest register with invalid type email should fail
    Register Android with invalid type email
Register : Guset register with doesn't fill email should fail
    Register Android with doesn't fill email
Register : Guset register with doesn't fill password should fail
    Register Android with doesn't fill password
Register : Guset register with doesn't fill email&password should fail
    Register Android with doesn't fill email&password

