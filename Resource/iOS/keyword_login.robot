*** Settings ***
Resource    ../../common.robot

*** Keywords ***
Login
    [Arguments]     ${email}    ${password}     ${message}
    Open Arbotena
    sleep  4
    click element  name=Profile
    sleep  1
    clear text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField
    clear text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeSecureTextField
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField    ${email}
    input password  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeSecureTextField    ${password}
    click element  name=LOGIN
    sleep   2
    wait until page contains element  name=${message}
    capture page screenshot

Logout
    Login   ${txt_email}    ${txt_password}     ${msg_success}
    click element  name=Sign out
    click element  name=JA
    wait until page contains element  name=LOGIN