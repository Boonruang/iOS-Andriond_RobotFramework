*** Settings ***
Resource    ../../common.robot

*** Keywords ***
show detail checklist
    Open Arbotena
    sleep  2
    click element  name=Quick Check 1
    sleep  1
    wait until page contains element    name=COST OF MEDICAL
    capture page screenshot

checked term&condition
    Login  noaddress@test.com    20Scoops    noaddress @test.com
    sleep  2
    click element  name=Checklist
    sleep  1
    click element  name=Quick Check 1
    sleep  1
    wait until page contains element    name=COST OF MEDICAL
    click element  name=SUBMIT CHECKLIST
    wait until page contains element    name=Term & Condition
    click element  name=ACCEPT
    click element  name=Accept Term & Condition
    click element  name=ACCEPT
    wait until page contains element  name=Are you ready for Question?
    capture page screenshot

without check term&condition
    Open Arbotena
    sleep  2
    click element  name=Quick Check 1
    sleep  1
    wait until page contains element    name=COST OF MEDICAL
    click element  name=SUBMIT CHECKLIST
    wait until page contains element    name=Term & Condition
    click element  name=ACCEPT
    click element  name=ACCEPT
    wait until page contains element  name=PLEASE CHECK CONFIRM AND CONDITION
    capture page screenshot

buy checklist
    Login  noaddress@test.com    20Scoops    noaddress @test.com
    sleep  2
    click element  name=Checklist
    sleep  1
    click element  name=Quick Check 1
    sleep  1
    wait until page contains element    name=COST OF MEDICAL
    click element  name=SUBMIT CHECKLIST
    wait until page contains element    name=Term & Condition
    click element  name=ACCEPT
    click element  name=Accept Term & Condition
    click element  name=ACCEPT
    wait until page contains element  name=Are you ready for Question?
    click element  name=I'M READY, START QUESTION
    wait until page contains element  name=Quick Check
    sleep  3
    :For    ${x}    IN RANGE    40
    \   click element  name=Ja
    wait until page contains element  name=Congratulations!


buy checklist not complete
    Login  test6@test.com    20Scoops    test6 @test.com
    click element  name=Checklist
    sleep  1
    click element  name=Quick Check 1
    sleep  1
    wait until page contains element    name=COST OF MEDICAL
    click element  name=SUBMIT CHECKLIST
    wait until page contains element    name=Term & Condition
    click element  name=ACCEPT
    click element  name=Accept Term & Condition
    click element  name=ACCEPT
    wait until page contains element  name=Are you ready for Question?
    click element  name=I'M READY, START QUESTION
    wait until page contains element  name=Quick Check
    sleep  2
    click element  name=Ja

buy checklist but doesn't checklist
    Login   test6@test.com    20Scoops    test6 @test.com
    click element  name=My Data
    sleep  1
    click element  name=nbv checklist
    sleep  1
    wait until page contains element  name=Quick Check
    sleep  1
    click element  name=nbv checklist
    wait until page contains element  name=Edit Check List

buy first checklist
    Login  2@test.com    20Scoops    2 @test.com
    sleep  1
    click element  name=My Data
    wait until page contains element    name=Quick Check 1


