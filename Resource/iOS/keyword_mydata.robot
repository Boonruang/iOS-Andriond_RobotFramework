*** Settings ***
Resource    ../../common.robot

*** Keywords ***
menu mydata without login
    Open Arbotena
    sleep  2
    click element  name=My Data
    wait until page contains element  name=NO INFORMATION

check mydata with login have't data
    Login  account@test.com  20Scoops  account @test.com
    click element  name=My Data
    wait until page contains element  name=NO INFORMATION

first enter mydata menu
    Login  h@h.com  20Scoops  h @h.com
    sleep  2
    click element  name=My Data
    sleep  1
    wait until page contains element    name=Your Information
    wait until page contains element    name=ICH VERSTEHE
    click element  name=ICH VERSTEHE
    wait until page contains element  name=Nahrungsmittelzusatzstoffe E-Nummern auf die Sie positiv reagiert
    capture page screenshot

seconde enter mydata menu
    Login  h@h.com  20Scoops  h @h.com
    sleep  2
    click element  name=My Data
    sleep  1
    wait until page contains element    name=Your Information
    wait until page contains element    name=ICH VERSTEHE
    click element  name=ICH VERSTEHE
    wait until page contains element  name=Nahrungsmittelzusatzstoffe E-Nummern auf die Sie positiv reagiert
    click element  name=Checklist
    wait until page contains element  name=Quick Check 1
    click element  name=My Data
    wait until page contains element  name=Nahrungsmittelzusatzstoffe E-Nummern auf die Sie positiv reagiert
    capture page screenshot

search state mydata menu
    Login  h@h.com  20Scoops  h @h.com
    sleep  2
    click element  name=My Data
    sleep  1
    wait until page contains element    name=Your Information
    wait until page contains element    name=ICH VERSTEHE
    click element  name=ICH VERSTEHE
    wait until page contains element  name=Nahrungsmittelzusatzstoffe E-Nummern auf die Sie positiv reagiert
    click element  name=Search your food list
    input text  name=Search your food list  am
    wait until page contains element    name=Amaranth E123

change category in mydata menu
    Login  h@h.com  20Scoops  h @h.com
    sleep  2
    click element  name=My Data
    sleep  1
    wait until page contains element    name=Your Information
    wait until page contains element    name=ICH VERSTEHE
    click element  name=ICH VERSTEHE
    wait until page contains element  name=Nahrungsmittelzusatzstoffe E-Nummern auf die Sie positiv reagiert
    click element  name=mdt category
    click element  name=Meidbare Lebensmittel
    wait until page contains element  name=Nahrungsmittel auf die Sie positiv reagiert

button hide in mydata
    Login  h@h.com  20Scoops  h @h.com
    sleep  2
    click element  name=My Data
    sleep  1
    wait until page contains element    name=Your Information
    wait until page contains element    name=ICH VERSTEHE
    click element  name=ICH VERSTEHE
    wait until page contains element  name=Nahrungsmittelzusatzstoffe E-Nummern auf die Sie positiv reagiert
    click element  xpath=(//XCUIElementTypeButton[@name="HIDE"])[1]
    wait until page does not contain element  name=Amaranth E123
