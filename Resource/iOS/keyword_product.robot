*** Settings ***
Resource    ../../common.robot

*** Keywords ***
show detail product&package
    Open Arbotena
    sleep  2
    click element  name=Product
    sleep  2
    wait until page contains element    name=Our products for health conscious people like you. Can choose the products that we offer to suit your needs.
    capture page screenshot
add product to cart
    Login   ${txt_email}    ${txt_password}    ${msg_success}
    sleep  2
    click element  name=Product
    sleep  2
    wait until page contains element    name=Our products for health conscious people like you. Can choose the products that we offer to suit your needs.
    click element  name=A-SPRAY No.1
    wait until page contains element  name=PRODUCT NAME
    click element  name=ADD TO CART
    wait until page contains element  name=1
    click element  name=nbv cart
    wait until page contains element  name=Your Cart
    wait until page contains element  name=A-SPRAY No.1

remove product to cart
    Login   ${txt_email}    ${txt_password}    ${msg_success}
    sleep  2
    click element  name=Product
    sleep  1
    wait until page contains element    name=Our products for health conscious people like you. Can choose the products that we offer to suit your needs.
    click element  name=nbv cart
    sleep  1
    wait until page contains element  name=A-SPRAY No.1
    click element  name=nbv close
    wait until page contains element  name=Are you sure ?
    click element  name=JA
    wait until page does not contain element  name=A-SPRAY No.1

checkout with no address
    Login   ${txt_email}    ${txt_password}    ${msg_success}
    sleep  2
    click element  name=Product
    sleep  2
    wait until page contains element    name=Our products for health conscious people like you. Can choose the products that we offer to suit your needs.
    click element  name=A-SPRAY No.1
    wait until page contains element  name=PRODUCT NAME
    click element  name=ADD TO CART
    wait until page contains element  name=1
    click element  name=nbv cart
    wait until page contains element  name=Your Cart
    wait until page contains element  name=A-SPRAY No.1
    click element  name=NEXT
    wait until page contains element  name=Shipping Address
    click element  name=PROCESS TO CHECKOUT
    wait until page contains element  name=Shipping Address

checkout with address
    Login   ${txt_email}    ${txt_password}    ${msg_success}
    sleep  2
    click element  name=Product
    sleep  2
    wait until page contains element    name=Our products for health conscious people like you. Can choose the products that we offer to suit your needs.
    click element  name=A-SPRAY No.1
    wait until page contains element  name=PRODUCT NAME
    click element  name=ADD TO CART
    wait until page contains element  name=1
    click element  name=nbv cart
    wait until page contains element  name=Your Cart
    wait until page contains element  name=A-SPRAY No.1
    click element  name=NEXT
    wait until page contains element  name=Shipping Address
    click element  name=Add new Address
    wait until page contains element  name=Add New Address Book
    add address
    click element  name=SAVE


add address
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeTextField    test1
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeTextField    test2
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeTextField    test3
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[6]/XCUIElementTypeTextField    test4
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[7]/XCUIElementTypeTextField    test5
    swipe   16  457  16    600
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[10]/XCUIElementTypeTextField    test6
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[11]/XCUIElementTypeTextField    test7
    input text  xpath=//XCUIElementTypeApplication[@name="DEV-Arbotena"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[12]/XCUIElementTypeTextField    test8

