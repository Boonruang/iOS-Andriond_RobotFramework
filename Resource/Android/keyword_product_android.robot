*** Settings ***
Resource    ../../common.robot

*** Keywords ***
buy product success
    sleep  5
    click element  com.twentyscoops.arbotena.dev:id/menu_product
    wait until page contains  	Product
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[2]/android.widget.TextView
    wait until page contains  Package
    click element  com.twentyscoops.arbotena.dev:id/imgItem
    wait until page contains    Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnAddToCart
    sleep  3
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  please check confirm and condition
    click element  id=com.twentyscoops.arbotena.dev:id/chTermCondition
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  Login
    input text  com.twentyscoops.arbotena.dev:id/tilEtEmail    productaddress@test.com
    input password  com.twentyscoops.arbotena.dev:id/tilEtPassword    20Scoops$
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnLogin
    click element  com.twentyscoops.arbotena.dev:id/btnLogin
    sleep   6
    wait until page contains element  com.twentyscoops.arbotena.dev:id/menuProduct
    click element  com.twentyscoops.arbotena.dev:id/menuProduct
    wait until page contains  Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnNext
    wait until page contains  grand total
    click element  com.twentyscoops.arbotena.dev:id/btnNext
    wait until page contains  	Select Shipping Methods
    sleep  2
    click element  com.twentyscoops.arbotena.dev:id/btnCheckout
    wait until page contains element  xpath=//android.widget.Button[@content-desc="Purchase"]
    click element  xpath=//android.webkit.WebView[@content-desc="Payment"]/android.view.View[5]/android.view.View[1]
    click element  xpath=//android.widget.Button[@content-desc="Purchase"]
    sleep  ${speed_payment}
    wait until page contains element  xpath=//android.view.View[@content-desc="Demo Bank"]
    input text  xpath=//android.webkit.WebView[@content-desc="Login zur Bank. Sofort GmbH - a Klarna Group Company"]/android.view.View[2]/android.view.View[2]/android.view.View[6]/android.widget.EditText  88888888
    input text  xpath=//android.webkit.WebView[@content-desc="Login zur Bank. Sofort GmbH - a Klarna Group Company"]/android.view.View[2]/android.view.View[2]/android.view.View[7]/android.widget.EditText  88888888
    Swipe By Percent     50    50    50    5
    sleep  3
    click element  xpath=//android.widget.Button[@content-desc="Weiter"]
    wait until page contains element  xpath=//android.view.View[@content-desc="Kontoauswahl"]
    Click A Point  70  1000  100
    sleep  3
    click element  xpath=//android.widget.Button[@content-desc="Weiter"]
    wait until page contains element  xpath=//android.view.View[@content-desc="Transaktion bestätigen"]
    input text  xpath=//android.webkit.WebView[@content-desc="TAN-Eingabe. Sofort GmbH - a Klarna Group Company"]/android.view.View[1]/android.view.View[2]/android.view.View[5]/android.widget.EditText    88888888
    Swipe By Percent     50    50    50    5
    sleep  3
    click element  xpath=//android.widget.Button[@content-desc="Weiter"]
    sleep  ${speed_payment}
    wait until page contains element  xpath=//android.view.View[@content-desc="Process to Shopping"]
    click element  xpath=//android.view.View[@content-desc="Process to Shopping"]




add product to cart android
    sleep  5
    click element  com.twentyscoops.arbotena.dev:id/menu_product
    wait until page contains  	Product
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[2]/android.widget.TextView
    wait until page contains  Package
    click element  com.twentyscoops.arbotena.dev:id/imgItem
    wait until page contains    Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnAddToCart
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  please check confirm and condition
    click element  id=com.twentyscoops.arbotena.dev:id/chTermCondition
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  Login
    input text  com.twentyscoops.arbotena.dev:id/tilEtEmail    product@test.com
    input password  com.twentyscoops.arbotena.dev:id/tilEtPassword    20Scoops$
    sleep   1
    click element  com.twentyscoops.arbotena.dev:id/btnLogin
    sleep   4
    click element  com.twentyscoops.arbotena.dev:id/menuProduct
    wait until page contains  Platinum


remove product from cart android
    sleep  5
    click element  com.twentyscoops.arbotena.dev:id/menu_product
    wait until page contains  	Product
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[2]/android.widget.TextView
    wait until page contains  Package
    click element  com.twentyscoops.arbotena.dev:id/imgItem
    wait until page contains    Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnAddToCart
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  please check confirm and condition
    click element  id=com.twentyscoops.arbotena.dev:id/chTermCondition
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  Login
    input text  com.twentyscoops.arbotena.dev:id/tilEtEmail    product@test.com
    input password  com.twentyscoops.arbotena.dev:id/tilEtPassword    20Scoops$
    sleep   1
    click element  com.twentyscoops.arbotena.dev:id/btnLogin
    sleep   4
    click element  com.twentyscoops.arbotena.dev:id/menuProduct
    wait until page contains  Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnDelete
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnNegative
    click element  com.twentyscoops.arbotena.dev:id/btnNegative
    wait until page does not contain  Platinum

checkout with no address android
    sleep  5
    click element  com.twentyscoops.arbotena.dev:id/menu_product
    wait until page contains  	Product
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[2]/android.widget.TextView
    wait until page contains  Package
    click element  com.twentyscoops.arbotena.dev:id/imgItem
    wait until page contains    Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnAddToCart
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  please check confirm and condition
    click element  id=com.twentyscoops.arbotena.dev:id/chTermCondition
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  Login
    input text  com.twentyscoops.arbotena.dev:id/tilEtEmail    product@test.com
    input password  com.twentyscoops.arbotena.dev:id/tilEtPassword    20Scoops$
    sleep   1
    click element  com.twentyscoops.arbotena.dev:id/btnLogin
    sleep   4
    wait until page contains element  com.twentyscoops.arbotena.dev:id/menuProduct
    click element  com.twentyscoops.arbotena.dev:id/menuProduct
    wait until page contains  Platinum
    click element  com.twentyscoops.arbotena.dev:id/btnNext
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnNext
    sleep  2
    click element  com.twentyscoops.arbotena.dev:id/btnNext
    sleep  6
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnNext
