*** Settings ***
Resource    ../../common.robot

*** Keywords ***
Register
    [Arguments]  ${email_register}  ${password_register}
    sleep  6
    click element  id=com.twentyscoops.arbotena.dev:id/menu_profile
    sleep  2
    click element   id=com.twentyscoops.arbotena.dev:id/tvRegister
    input text  id=com.twentyscoops.arbotena.dev:id/tilEtEmail    ${email_register}
    input text  id=com.twentyscoops.arbotena.dev:id/tilEtPassword    ${password_register}
    sleep  1
    Swipe By Percent     50    50    50    5
    sleep  2
    click element  id=com.twentyscoops.arbotena.dev:id/rbMale
    click element  id=com.twentyscoops.arbotena.dev:id/tvBloodType
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  id=com.twentyscoops.arbotena.dev:id/btnNegative
    click element  id=com.twentyscoops.arbotena.dev:id/tvAge
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  id=com.twentyscoops.arbotena.dev:id/btnNegative
    click element  id=com.twentyscoops.arbotena.dev:id/tvWeight
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  id=com.twentyscoops.arbotena.dev:id/btnNegative
    click element  id=com.twentyscoops.arbotena.dev:id/tvHeight
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  id=com.twentyscoops.arbotena.dev:id/btnNegative
    sleep  1
    click element  id=com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android Complete
    ${random} =	Evaluate	random.randint(0, sys.maxint)	modules=random, sys
    Register    ${random}@test.com    20Scopps$
    sleep  6
    wait until page contains element  id=com.twentyscoops.arbotena.dev:id/tvEmail

Register Android does't fill field
    sleep  6
    click element  id=com.twentyscoops.arbotena.dev:id/menu_profile
    sleep  4
    click element   id=com.twentyscoops.arbotena.dev:id/tvRegister
    click element  id=com.twentyscoops.arbotena.dev:id/btnConfirm
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android duplicate email
    Register    sniperaek@gmail.com    20Scopps$
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android with password less than 6 digit
    Register    testAnwwwww@test.com    test
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android with password more than 12 digit
    Register    testAnwwww@test.com    xxxxxxxxxxxxxxxxxxxxx
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android with invalid type email
    Register    testAnwwww    20Scoops
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android with doesn't fill email
    Register    ${EMPTY}    20Scoops
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android with doesn't fill password
    Register    test223@test.com    ${EMPTY}
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm

Register Android with doesn't fill email&password
    Register    ${EMPTY}    ${EMPTY}
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnConfirm