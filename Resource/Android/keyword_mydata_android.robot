*** Settings ***
Resource    ../../common.robot

*** Keywords ***
menu mydata without login android
    sleep  3
    click element  com.twentyscoops.arbotena.dev:id/menu_my_data
    sleep  1
    wait until page contains    NO INFORMATION

check mydata with login have't data android
    login android  mydata@test.com  20Scoops$  ${anmsg_success}
    click element  com.twentyscoops.arbotena.dev:id/menu_my_data
    sleep  1
    wait until page contains    NO INFORMATION

first enter mydata menu android
    login android  sniperaek@gmail.com  20Scoops$  ${anmsg_success}
    click element  com.twentyscoops.arbotena.dev:id/menu_my_data
    wait until page contains    You Information
    wait until page contains element  id=com.twentyscoops.arbotena.dev:id/btnDismiss

search state mydata menu android
    login android  sniperaek@gmail.com  20Scoops$  ${anmsg_success}
    click element  com.twentyscoops.arbotena.dev:id/menu_my_data
    wait until page contains    You Information
    click element  com.twentyscoops.arbotena.dev:id/btnDismiss
    input text  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.TextView  am
    wait until page contains  Amarant E-123
    click element  com.twentyscoops.arbotena.dev:id/btnClose
    click element  com.twentyscoops.arbotena.dev:id/category
    sleep  1
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.TextView
    sleep  1
    input text  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.TextView  an
    wait until page contains  Ananas
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView
    wait until page does not contain  Amarant E-123
    click element  com.twentyscoops.arbotena.dev:id/menuMyInformation
    wait until page contains  Obst/Beeren


