*** Settings ***
Resource    ../../common.robot

*** Keywords ***
Login Android
    [Arguments]     ${antxt_email}    ${antxt_password}     ${anmsg_success}
    sleep  6
    click element  id=com.twentyscoops.arbotena.dev:id/menu_profile
    sleep  4
    input text  id=com.twentyscoops.arbotena.dev:id/tilEtEmail    ${antxt_email}
    input password  id=com.twentyscoops.arbotena.dev:id/tilEtPassword    ${antxt_password}
    sleep   1
    click element  id=com.twentyscoops.arbotena.dev:id/btnLogin
    sleep   5
    wait until page contains element  id=${anmsg_success}
    capture page screenshot

Logout Android
    [Arguments]     ${antxt_email}    ${antxt_password}     ${anmsg_success}
    Login Android    ${antxt_email}   ${antxt_password}    ${anmsg_success}
    sleep   2
    click element   xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[5]
    click element   xpath=/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button[1]
    wait until page contains  Please Login