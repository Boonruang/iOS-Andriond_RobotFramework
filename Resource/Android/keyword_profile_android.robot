*** Settings ***
Resource    ../../common.robot

*** Keywords ***
update profile
    login android  testchecklist@test.com  20Scoops$  ${anmsg_success}
    sleep  1
    click element  com.twentyscoops.arbotena.dev:id/menuMyInformation
    wait until page contains  Personal Information
    Swipe By Percent     50    50    50    10
    click element  com.twentyscoops.arbotena.dev:id/rbMale
    click element  com.twentyscoops.arbotena.dev:id/tvBloodType
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  com.twentyscoops.arbotena.dev:id/btnNegative
    click element  com.twentyscoops.arbotena.dev:id/tvAge
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  com.twentyscoops.arbotena.dev:id/btnNegative
    click element  com.twentyscoops.arbotena.dev:id/tvWeight
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  com.twentyscoops.arbotena.dev:id/btnNegative
    click element  com.twentyscoops.arbotena.dev:id/tvHeight
    sleep  1
    Swipe By Percent     50    50    30    50
    click element  com.twentyscoops.arbotena.dev:id/btnNegative
    sleep  1
    click element  com.twentyscoops.arbotena.dev:id/btnConfirm
    click element  com.twentyscoops.arbotena.dev:id/menuMyInformation

add address android
    login android  testchecklist@test.com  20Scoops$  ${anmsg_success}
    sleep  1
    click element  com.twentyscoops.arbotena.dev:id/menuAddressBook
    click element  com.twentyscoops.arbotena.dev:id/tvAddNewAddress
    input text  com.twentyscoops.arbotena.dev:id/tilEtFirstName  testA
    input text  com.twentyscoops.arbotena.dev:id/tilEtLastName  testB
    input text  com.twentyscoops.arbotena.dev:id/tilEtCompany  testC
    input text  com.twentyscoops.arbotena.dev:id/tilEtPhone  011111
    input text  com.twentyscoops.arbotena.dev:id/tilEtFax  10101
    sleep  1
    Swipe By Percent     50    50    50    1
    sleep  1
    input text  com.twentyscoops.arbotena.dev:id/tilEtStreetAddress  m.5
    input text  com.twentyscoops.arbotena.dev:id/tilEtCity  thai
    input text  com.twentyscoops.arbotena.dev:id/tilEtZipCode  57130
    click element  com.twentyscoops.arbotena.dev:id/btnSave
    wait until page contains    Default Billing Address

check order in profile menu
    login android  sniperaek@gmail.com  20Scoops$  ${anmsg_success}
    sleep  1
    click element  com.twentyscoops.arbotena.dev:id/menuMyOrder
    wait until page contains  16 Nov 2017
    click element  xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout
    wait until page contains  sniperaek@gmail.com



