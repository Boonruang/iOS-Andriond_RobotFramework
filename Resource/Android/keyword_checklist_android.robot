*** Settings ***
Resource    ../../common.robot

*** Keywords ***
payment
    [Arguments]  ${email}  ${password}
    Login Android  ${email}  ${password}  ${anmsg_success}
    click element  id=com.twentyscoops.arbotena.dev:id/menu_check_list
    sleep  1
    wait until page contains  Quick Check 1
    click element  id=com.twentyscoops.arbotena.dev:id/tvItemName
    wait until page contains  Cost of medical
    sleep  4
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnSubmit
    click element  id=com.twentyscoops.arbotena.dev:id/btnSubmit
    sleep  3
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnAccept
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  please check confirm and condition
    click element  id=com.twentyscoops.arbotena.dev:id/chTermCondition
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  Are you ready for Question?
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnStart
    click element  id=com.twentyscoops.arbotena.dev:id/btnStart
    sleep  8
    wait until page contains element  xpath=//android.widget.Button[@content-desc="Purchase"]
    click element  xpath=//android.webkit.WebView[@content-desc="Payment"]/android.view.View[5]/android.view.View[1]
    click element  xpath=//android.widget.Button[@content-desc="Purchase"]
    sleep  ${speed_payment}
    wait until page contains element  xpath=//android.view.View[@content-desc="Demo Bank"]
    input text  xpath=//android.webkit.WebView[@content-desc="Login zur Bank. Sofort GmbH - a Klarna Group Company"]/android.view.View[2]/android.view.View[2]/android.view.View[6]/android.widget.EditText  88888888
    input text  xpath=//android.webkit.WebView[@content-desc="Login zur Bank. Sofort GmbH - a Klarna Group Company"]/android.view.View[2]/android.view.View[2]/android.view.View[7]/android.widget.EditText  88888888
    Swipe By Percent     50    50    50    5
    sleep  3
    click element  xpath=//android.widget.Button[@content-desc="Weiter"]
    wait until page contains element  xpath=//android.view.View[@content-desc="Kontoauswahl"]
    Click A Point  70  1000  100
    sleep  3
    click element  xpath=//android.widget.Button[@content-desc="Weiter"]
    wait until page contains element  xpath=//android.view.View[@content-desc="Transaktion bestätigen"]
    input text  xpath=//android.webkit.WebView[@content-desc="TAN-Eingabe. Sofort GmbH - a Klarna Group Company"]/android.view.View[1]/android.view.View[2]/android.view.View[5]/android.widget.EditText    88888888
    Swipe By Percent     50    50    50    5
    sleep  3
    click element  xpath=//android.widget.Button[@content-desc="Weiter"]
    sleep  ${speed_payment}
    wait until page contains element  xpath=//android.view.View[@content-desc="Process to Shopping"]
    click element  xpath=//android.view.View[@content-desc="Process to Shopping"]
    wait until page contains element  id=com.twentyscoops.arbotena.dev:id/tvTbTitle

buy checklist without login
    sleep  4
    Swipe By Percent     50    50    50    40
    click element  id=com.twentyscoops.arbotena.dev:id/tvItemName
    sleep  1
    wait until page contains  Submit Checklist
    click element  id=com.twentyscoops.arbotena.dev:id/btnSubmit
    sleep  3
    wait until page contains element  com.twentyscoops.arbotena.dev:id/btnAccept
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  please check confirm and condition
    click element  id=com.twentyscoops.arbotena.dev:id/chTermCondition
    click element  id=com.twentyscoops.arbotena.dev:id/btnAccept
    wait until page contains  Login


buy checklist android
    sleep  2
    payment  testchecklist@test.com  20Scoops$
    sleep  2
    :For    ${x}    IN RANGE    40
    \   click element  id=com.twentyscoops.arbotena.dev:id/btnNo
    wait until page contains  Congratulations!

buy checklist not complete android
    sleep  2
    payment  testchecklist3@test.com  20Scoops$
    sleep  2
    click element  id=com.twentyscoops.arbotena.dev:id/btnNo
    sleep  1

comback does checklist not complete android
    sleep  2
    login android  testchecklist3@test.com  20Scoops$  ${anmsg_success}
    click element  id=com.twentyscoops.arbotena.dev:id/menu_my_data
    sleep  3
    wait until page contains  com.twentyscoops.arbotena.dev:id/menuContinueQuick
    click element  com.twentyscoops.arbotena.dev:id/menuContinueQuick
    sleep  1
    wait until page contains    Quick Check
    sleep  2
    :For    ${x}    IN RANGE    39
    \   click element  id=com.twentyscoops.arbotena.dev:id/btnNo
    wait until page contains  Congratulations!

firsttime buy checklist
    sleep  2
    login android  firsttime@test.com  20Scoops  ${anmsg_success}
    click element  id=com.twentyscoops.arbotena.dev:id/menu_my_data
    sleep  1
